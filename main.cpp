#include <stdlib.h>  //srand()
#include <iostream>  //cin, cout
#include <string>    //string
#include <stdio.h>  //FILE *
#include <ctime>    //time(0)

#define NR_INTREBARI 7

using namespace std;

struct intrebare
{
    string enunt;   ///am pus si enuntul si variantele de raspuns intr-un singur string
    char var_cor;   ///varianta corecta, separat
};

int main()
{

    intrebare question; /// nu mai salvam toate intrebarile intr-un vector de intrebari pentru ca se ocupa prea multa memorie.
                        ///salvam intr-o singura intrebare pe care o rescriem mereu cand vine o intrebare noua.


    int intrebariFolosite[NR_INTREBARI];   ///aici salvam intr-un vector de int ce intrebari au fost puse deja
                                            /// 0 inseamna nefolosit, 1 inseamna folosit
    for(int i=0;i<NR_INTREBARI;i++)
        intrebariFolosite[i]=0;     //initializam cu 0




    char buf;   ///bufferul cu care citim din fisier
    string sbuf;  ///bufferul in care salvam toata intrebarea



    FILE * input = fopen("Questions.txt","rb"); ///atentie: foloseste notepad++ ... apar \n -uri in fisier pe care nu le vezi
    if(!input)     ///daca da eroare la citire, sa stim ca programul returneaza -1
        return -1;



    int castig=0;
    int pondere=5;


    srand(time(0));




                ///incepem jocul

    for(int i=0;i<NR_INTREBARI;i++)
    {



        int rnd = rand()%NR_INTREBARI;  ///determinam a cata intrebare s-o punem

        while(intrebariFolosite[rnd] != 0) ///daca a fost pusa deja, mai cauta alta intrebare
        {
            rnd = rand()%NR_INTREBARI;
        }
        intrebariFolosite[rnd]=1;   ///cand gasim o intrebare care nu a fost pusa, o marcam cum ca a fost aleasa

        rnd+=49;  ///uita-te in tabela ascii ce este intre 49+0 si 49+NR_INTREBARI ( dec 49 -> chr 1, dec 50 -> chr 2, ...)

        char rnd_s = char(rnd);  ///transformam din int 3 in char '3' (exemplu ... putea sa fie orice numar intre 0 si 6)





        fseek(input,0,SEEK_SET);    ///incepem sa citim de la inceputul fisierului (SEEK_SET = inceputul fisierului si 0 = distanta in caractere de la SEEK_SET)
        fread(&buf,sizeof(char),1,input); ///citim primul caracter din fisier

        while(buf!=rnd_s)   ///comparam buf cu caracterul '3'
        {
            fread(&buf,sizeof(char),1,input);   ///daca e diferit de '3', citim in continuare pana dam de '3'  --> atentia sa nu apara cifre in variantele de raspuns: 50 Hz, Albereta 70
        }



        while(true)   ///programul merge la infinit
        {
            if(buf=='@')    ///sfarsitul fisierului -> iesim din infinite loop
                break;


            sbuf = sbuf+buf;    ///completam intrebarea


            fread(&buf,sizeof(char),1,input); /// citim urmatorul caracter
            if(buf=='_')  //ca sa identific in fisier care este varianta corecta
            {
                question.enunt=sbuf; ///salvez tot enuntul pe care o sa-l arat jucatorului
                fread(&buf,sizeof(char),1,input);  //citesc varianta corecta
                question.var_cor = buf;  ///pun varianta corecta in structura ca apoi playerul sa o ghiceasca

                sbuf = "";  //golesc stringul in care o sa salvez urmatoarele enunturi

                break;  ///iesim din infinite loop ca sa putem pune intrebarea
            }
        }

        cout<<question.enunt;

        char optiune;
        cin>>optiune;

        if(optiune == question.var_cor)
        {
            cout<<"E bine\n";
            castig+=pondere;
            cout<<"Castigul tau acuma este "<<castig<<'\n';
        }

        else
        {
            cout<<"Ai perdut\n";
            cout<<"Ai plecat cu "<<castig - 15;

            fclose(input);
            return 0;
        }
    }       /// --------------------------------------repetam pana cand consumam toate intrebarile din fisier   ----------------------------------------------------

    fclose(input);
    return 0;
}



